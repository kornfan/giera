﻿using UnityEngine;
using System.Collections;

public class PlatformTrigger : MonoBehaviour {


	public GameObject hero;

	private BoxCollider2D boxCollider;
	private BoxCollider2D playerCollider;
	// Use this for initialization
	void Start () {
		boxCollider = GetComponent<BoxCollider2D> ();
		playerCollider = hero.GetComponent<BoxCollider2D> ();
	}
	
	void OnTriggerEnter2D(Collider2D other)
	{
		Physics2D.IgnoreCollision (boxCollider, playerCollider, true);
		Debug.Log ("Trigger ON");
	}

	void OnTriggerExit2D(Collider2D other)
	{
		Physics2D.IgnoreCollision (boxCollider, playerCollider, false);
		Debug.Log ("Trigger OFF");
	}
}
