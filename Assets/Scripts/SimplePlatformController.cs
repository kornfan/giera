using UnityEngine;
using System.Collections;

public class SimplePlatformController : MonoBehaviour {

	[HideInInspector] public bool facingRight = true;
	[HideInInspector] public bool jump = false;

	public float moveForce = 365.0f;
	public float maxSpeedX = 5f;
	public float maxSpeedY = 20f;
	public float jumpForce = 1000f;
	public Transform groundCheckRight;
	private bool groundedLeft = false;
	private bool groundedRight = false;
	private Animator anim;
	private Rigidbody2D rb2d;
	private bool canJump = false;
	private GameObject cameraObj;
	private float maxHeight = 0;
	// Use this for initialization
	void Awake () {
		anim = GetComponent <Animator>();
		rb2d = GetComponent<Rigidbody2D>();
		cameraObj = GameObject.FindGameObjectWithTag("MainCamera");
	}
	
	// Update is called once per frame
	void Update () {
		if (transform.position.y > maxHeight) {
			cameraObj.transform.position = new Vector3 (cameraObj.transform.position.x, transform.position.y, cameraObj.transform.position.z);
			maxHeight = transform.position.y;
		}

        
        //groundedRight = Physics2D.Linecast(transform.position, groundCheckRight.position, 1 << LayerMask.NameToLayer("Ground"));

        //if (Input.GetKeyDown(KeyCode.Space) && (groundedRight))
        //{
        //    jump = true;
        //}
        //else
        //{
        //    jump = false;
        //}

    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        //Debug.Log ("onCollisionEnter");

        Collider2D collider = collision.collider;

        if (collider.tag == "ground")
        {
            //Debug.Log ("---if");
            Vector3 contactPoint = collision.contacts[0].point;
            Vector3 center = collider.bounds.center;

            bool top = contactPoint.y > center.y;
            //bool goingUp = rb2d.velocity.y > 0 ;
            if (top)
            {
                jump = true;
                //Debug.Log ("jump true");
            } else
              Debug.Log ("jump false");
        }

    }

    void FixedUpdate()
	{
		float h = Input.GetAxis ("Horizontal");
		anim.SetFloat ("Speed", Mathf.Abs (h));
		if (h * rb2d.velocity.x < maxSpeedX)
			rb2d.AddForce (Vector2.right * h * moveForce);
		if (Mathf.Abs (rb2d.velocity.x) > maxSpeedX)
			rb2d.velocity = new Vector2 (Mathf.Sign (rb2d.velocity.x) * maxSpeedX, rb2d.velocity.y);
		if (h > 0 && !facingRight)
			Flip ();
 		else if (h < 0 && facingRight)
			Flip ();
		if (jump) {
			anim.SetTrigger ("Jump"); 
			rb2d.velocity = new Vector2 (rb2d.velocity.x, maxSpeedY);
			jump = false;
			//Debug.Log("fixed jump");
		}
	}

	void Flip()
	{
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
}
