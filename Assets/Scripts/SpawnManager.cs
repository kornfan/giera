﻿using UnityEngine;
using System.Collections;

public class SpawnManager : MonoBehaviour {

	public GameObject platform;
	public float horizontalMin = -14f;
	public float horizontalMax = 14f;
	public float verticalMin = 4f;
	public float verticalMax = 6f;

	private static Vector2 originPosition;
	private static int numer = 1;
	private int objId = 0;
	// Use this for initialization
	void Start () {
		originPosition = transform.position;
	}
	
	void OnBecameVisible()
	{
		Debug.Log ("onBecameVisible" + numer);
		Vector2 randomPosition =  new Vector2(Random.Range(horizontalMin,horizontalMax),originPosition.y + Random.Range (verticalMin,verticalMax));
		Object clone = Instantiate(platform,randomPosition, Quaternion.identity);
		objId = numer++;
		clone.name = "Ground " + objId;

		originPosition.y = randomPosition.y;
	}

	void OnBecameInvisible()
	{
		Destroy (gameObject);
	}
}
